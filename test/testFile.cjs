const { error } = require('console');
const {dataBasedOnId, groupingBasedOnCompanies,dataOfParticularCompany,removingDataOfParticularCompany,sortingData,swapingBasedOnPsition,addingBirthDate}=require('../employeeDrillSoluton.cjs');

async function employees(){

    try{
        let dataBased=await dataBasedOnId("data.json");
        console.log(dataBased);
    }catch{
        console.log(error)
    }

    try{
        let companyGroups=await groupingBasedOnCompanies("data.json");
        console.log(companyGroups);
    }catch{
        console.log(error)
    }

    try{
        let dataOfParticular=await dataOfParticularCompany("data.json");
        console.log(dataOfParticular);
    }catch{
        console.log(error)
    }

    try{
        let removeParticularCompany=await removingDataOfParticularCompany("data.json");
        console.log(removeParticularCompany);
    }catch{
        console.log(error)
    }

    try{
        let sorting=await sortingData("data.json");
        console.log(sorting);
    }catch{
        console.log(error)
    }

    try{
        let swappedPositions=await swapingBasedOnPsition("data.json");
        console.log(swappedPositions);
    }catch{
        console.log(error)
    }

    try{
        let birthDate=await addingBirthDate("data.json");
        console.log(birthDate);
    }catch{
        console.log(error)
    }
}

employees()